$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "workarea/plugin_b/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "workarea-plugin_b"
  s.version     = Workarea::PluginB::VERSION
  s.authors     = ["Chris Cressman"]
  s.email       = ["ccressman@weblinc.com"]
  s.summary     = "This is the summary"
  s.description = "This is the description"
  
  s.files = `git ls-files`.split("\n")
  end
