require 'workarea/testing/teaspoon'

Teaspoon.configure do |config|
  config.root = Workarea::PluginA::Engine.root
  Workarea::Teaspoon.apply(config)
end
