module Workarea
  module PluginA
    class Engine < ::Rails::Engine
      include Workarea::Plugin
      isolate_namespace Workarea::PluginA

      File.open('/vagrant/log', 'a') do |log|
        log.puts("#{__FILE__} - top of class definition")
      end

      initializer 'PluginA.engine.rb' do
        File.open('/vagrant/log', 'a') do |log|
          log.puts("#{__FILE__} - Railtie#initializer block")
        end
      end

      config.before_configuration do
        File.open('/vagrant/log', 'a') do |log|
          log.puts("#{__FILE__} - before_configuration block")
        end
      end

      config.before_initialize do
        File.open('/vagrant/log', 'a') do |log|
          log.puts("#{__FILE__} - before_initialize block")
        end
      end

      config.to_prepare do
        File.open('/vagrant/log', 'a') do |log|
          log.puts("#{__FILE__} - to_prepare block")
        end
      end

      config.before_eager_load do
        File.open('/vagrant/log', 'a') do |log|
          log.puts("#{__FILE__} - before_eager_load block")
        end
      end

      config.after_initialize do
        File.open('/vagrant/log', 'a') do |log|
          log.puts("#{__FILE__} - after_initialize block")
        end
      end
    end
  end
end
