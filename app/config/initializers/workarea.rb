Workarea.configure do |config|
  # Basic site info
  config.site_name = 'App'
  config.host = 'app.dev'
  config.email_to = 'customerservice@app.dev'
  config.email_from = 'noreply@app.dev'
end
