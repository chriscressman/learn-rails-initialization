require 'workarea/testing/teaspoon'

Teaspoon.configure do |config|
  config.root = Workarea::PluginC::Engine.root
  Workarea::Teaspoon.apply(config)
end
